* Composants basiques :
    - View
    - Text
    - Image
    - TextInput
        -> 'placeholder' pour une valeur prédéfinie
    - ScrollView
    - StyleSheet
---------------------------------------------------------------------------------------------------
* Composants interface utilisateur
    - Button
        -> propriétés 'title' et 'onPress' obligatoires
    - Picker
    - Slider
    - Switch
---------------------------------------------------------------------------------------------------
* Composants de listes
    - FlatList
        -> propriétés 'data' et 'renderItem' obligatoires
        -> définition d'une clé sur les items de la liste : keyExtractor={(item) => item.id.toString()}
    - SectionList
---------------------------------------------------------------------------------------------------
* Composants spécifiques iOS
    - ActionSheetIOS
    - AlertIOS
    - DatePickerIOS
    - ImagePickerIOS
    - NavigatorIOS
    - ProgressViewIOS
    - PushNotificationsIOS
    - SegmentedControlIOS
    - TabBarIOS
---------------------------------------------------------------------------------------------------
* Composants spécifiques Android
    - BackHandler
    - DatePickerAndroid
    - DrawerLayoutAndroid
    - PermissionsAndroid
    - ProgressBarAndroid
    - TimePickerAndroid
    - ToastAndroid
    - ToolbarAndroid
    - ViewPagerAndroid
---------------------------------------------------------------------------------------------------
* Autres composants
    - ActivityIndicator
    - Alert
    - Animated
    - CameraRoll
    - ClipBoard
    - Dimensions
    - KeyboardAvoidingView
    - Linking
    - Modal
    - PixelRatio
    - RefreshControl
    - StatusBar
    - WebView
--------------------------------------------------------------------------------------------------- 