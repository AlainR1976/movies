import React from 'react'
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native'
import { getImageFromApi } from '../API/TMDBApi' // import { } from ... car c'est un export nommé dans TMDBApi.js
import FadeIn from '../Animations/FadeIn'

class FilmItem extends React.Component{
 
  _displayFavoriteImage() {
    if (this.props.isFilmFavorite) {
      // Si la props isFilmFavorite vaut true, on affiche le 🖤
      return (
        <Image
          style={styles.favorite_image}
          source={require('../Images/ic_favorite.png')}
        />
      )
    }
  }

  render() {
    const {film, displayDetailForFilm} = this.props

    return(
      <FadeIn>
        <TouchableOpacity style={styles.main_container} onPress={()=>displayDetailForFilm(film.id)}>
          <Image
            style={styles.image_container}
            source={{uri: getImageFromApi(this.props.film.poster_path)}}
          />
          <View style={styles.content_view}>
            <View style={styles.header_view}>
              {this._displayFavoriteImage()}
              <Text style={styles.title_text}>{film.original_title}</Text>
              <Text style={styles.vote_text}>{film.vote_average}</Text>
            </View>
            <View style={styles.description_view}>
              <Text style={styles.description_text} numberOfLines={6}>{film.overview}</Text>
            </View>
            <View style={styles.date_view}>
              <Text style={styles.date_text}>Sorti le {film.release_date}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </FadeIn>
    )
  }
}

const styles = StyleSheet.create(
  {
      main_container:{
        flexDirection : 'row',
        height: 190,
        margin:5,
        padding:5
      },
      content_view:{
        flex:1,
        margin:5,
        flexDirection : 'column'
      },
      image_container:{
        width: 120,
        height: 180,
        margin: 5,
        backgroundColor: 'grey'
      },
      header_view:{
        flexDirection : 'row',
        flex:3
      },
      title_text:{
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
      },
      vote_text:{
        fontWeight: 'bold',
        fontSize: 26,
        color: '#666666'
      },
      description_view:{
        flex:7
      },
      description_text:{
        fontStyle: 'italic',
        color:'#666666'
      },
      date_view:{
        flex:1
      },
      date_text:{
        textAlign:'right',
        fontSize:14
      },
      favorite_image:{
        height: 25,
        width: 25,
        marginRight: 5
      }
  }
)

export default FilmItem
