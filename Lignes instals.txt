1 - Installation de Node.js
2 - $ npm install -g expo-cli //Installation de expo
3 - Dans le dossier ReactNative : $ expo init MoviesAndMe // Initialise le projet - Crée les sous-dossiers nécessaires
4 - $ npm start //Démarre le serveur expo
    4bis - Lancer l'émulateur Android via Android Studio
5 - $ npm install --save react-navigation // Installation des librairies React Navigation (Pour naviguer entre les vues)
6 - $ npm install --save react-native-gesture-handler //Installation des librairies du gesture handler
